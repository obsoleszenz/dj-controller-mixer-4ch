#include "MidiPotentiometer.h"

MidiPotentiometer::MidiPotentiometer(int pin, int control, int channel) : Potentiometer(pin), control(control), channel(channel) {}

void MidiPotentiometer::onChange(int value, bool force_send_midi) { 
  int midiValue = map(value, 0, 1023, 0, 127);
  if (!force_send_midi && midiValue == lastMidiValue) return;
  lastMidiValue = midiValue;

  DBG("MidiPotentiometer [%i:%i] %i", channel, control, midiValue);
  sendMIDI(MIDI_COMMAND_POTI, channel, control, midiValue);
}

int MidiPotentiometer::value() {
  return lastMidiValue;
}
