#pragma once

#include <Arduino.h>

#include "hardware/TwoWaySwitch.h"
#include "log.h"
#include "sendMidi.h"
#include "constants.h"

class MidiTwoWaySwitch : public TwoWaySwitch {
  int channel;
  int control;

  public:
    MidiTwoWaySwitch(int pin_switch, int channel, int control);
    void onActivate();
    void onDeactivate();
};
