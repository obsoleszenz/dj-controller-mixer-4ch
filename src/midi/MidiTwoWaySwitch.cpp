#include "MidiTwoWaySwitch.h"

MidiTwoWaySwitch::MidiTwoWaySwitch(int pin_switch, int control, int channel) : TwoWaySwitch(pin_switch), control(control), channel(channel) {}

void MidiTwoWaySwitch::onActivate() {
  IFDEBUG(
    Serial.println(String("MidiTwoWaySwitch [") + channel + ":" + control + "] onActivate")
  );
  sendMIDI(MIDI_COMMAND_BUTTON, channel, control, MIDI_VALUE_BUTTON_PRESSED);
}

void MidiTwoWaySwitch::onDeactivate() {
  IFDEBUG(
    Serial.println(String("MidiTwoWaySwitch [") + channel + ":" + control + "] onDeactivate")
  );
  sendMIDI(MIDI_COMMAND_BUTTON, channel, control, MIDI_VALUE_BUTTON_RELEASE);
}
