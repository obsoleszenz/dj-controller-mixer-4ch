#include "MidiButton.h"

MidiButton::MidiButton(int pin_button, int control, int channel) : Button(pin_button), control(control), channel(channel) {}

void MidiButton::onPress() {
  DBG("MidiButton [%i:%i] Pressed!", channel, control);
  sendMIDI(MIDI_COMMAND_BUTTON, channel, control, MIDI_VALUE_BUTTON_PRESSED);
}

void MidiButton::onRelease() {
  DBG("MidiButton [%i:%i] Released!", channel, control);
  sendMIDI(MIDI_COMMAND_BUTTON, channel, control, MIDI_VALUE_BUTTON_RELEASE);
}
