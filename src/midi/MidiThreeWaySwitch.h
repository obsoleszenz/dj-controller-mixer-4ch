#pragma once

#include <Arduino.h>

#include "hardware/ThreeWaySwitch.h"
#include "log.h"
#include "sendMidi.h"
#include "constants.h"

class MidiThreeWaySwitch : public ThreeWaySwitch {
  int channel;
  int control;

  public:
    MidiThreeWaySwitch(int pin_switch_a, int pin_switch_b, int channel, int control);
    void onStateOne();
    void onStateTwo();
    void onStateThree();
};
