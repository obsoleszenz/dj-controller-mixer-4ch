#include "MidiThreeWaySwitch.h"

MidiThreeWaySwitch::MidiThreeWaySwitch(int pin_switch_a, int pin_switch_b, int control, int channel) : ThreeWaySwitch(pin_switch_a, pin_switch_b), control(control), channel(channel) {}

void MidiThreeWaySwitch::onStateOne() {
  IFDEBUG(
    Serial.println(String("MidiThreeWaySwitch [") + channel + ":" + control + "] StateOne")
  );
  sendMIDI(MIDI_COMMAND_BUTTON, channel, control, MIDI_VALUE_THREE_WAY_SWITCH_ONE);
}

void MidiThreeWaySwitch::onStateTwo() {
  IFDEBUG(
    Serial.println(String("MidiThreeWaySwitch [") + channel + ":" + control + "] StateTwo")
  );
  sendMIDI(MIDI_COMMAND_BUTTON, channel, control, MIDI_VALUE_THREE_WAY_SWITCH_TWO);
}

void MidiThreeWaySwitch::onStateThree() {
  IFDEBUG(
    Serial.println(String("MidiThreeWaySwitch [") + channel + ":" + control + "] StateThree")
  );
  sendMIDI(MIDI_COMMAND_BUTTON, channel, control, MIDI_VALUE_THREE_WAY_SWITCH_THREE);
}
