#include "main.h"

Multiplexer* multiplexer = new Multiplexer();
BaseComponent* components_multiplexer_a[] = {
  new MidiPotentiometer(PIN_MULTIPLEXER_A, MIDI_CTRL_FX1_SEND, MIDI_CHANNEL_ONE),
  new MidiPotentiometer(PIN_MULTIPLEXER_A, MIDI_CTRL_FX1_SEND, MIDI_CHANNEL_TWO),
  new MidiPotentiometer(PIN_MULTIPLEXER_A, MIDI_CTRL_FX1_SEND, MIDI_CHANNEL_THREE),
  new MidiPotentiometer(PIN_MULTIPLEXER_A, MIDI_CTRL_FX1_SEND, MIDI_CHANNEL_FOUR),
  new MidiPotentiometer(PIN_MULTIPLEXER_A, MIDI_CTRL_GAIN, MIDI_CHANNEL_FIVE),
  new MidiPotentiometer(PIN_MULTIPLEXER_A, MIDI_CTRL_FX2_SEND, MIDI_CHANNEL_ONE),
  new MidiPotentiometer(PIN_MULTIPLEXER_A, MIDI_CTRL_FX2_SEND, MIDI_CHANNEL_TWO),
  new MidiPotentiometer(PIN_MULTIPLEXER_A, MIDI_CTRL_FX2_SEND, MIDI_CHANNEL_THREE),
  new MidiPotentiometer(PIN_MULTIPLEXER_A, MIDI_CTRL_FX2_SEND, MIDI_CHANNEL_FOUR),
  new MidiPotentiometer(PIN_MULTIPLEXER_A, MIDI_CTRL_GAIN, MIDI_CHANNEL_SIX),
  new MidiPotentiometer(PIN_MULTIPLEXER_A, MIDI_CTRL_GAIN, MIDI_CHANNEL_MAIN),
  new MidiPotentiometer(PIN_MULTIPLEXER_A, MIDI_CTRL_GAIN, MIDI_CHANNEL_FOUR),
  new MidiPotentiometer(PIN_MULTIPLEXER_A, MIDI_CTRL_GAIN, MIDI_CHANNEL_THREE),
  new MidiPotentiometer(PIN_MULTIPLEXER_A, MIDI_CTRL_GAIN, MIDI_CHANNEL_TWO),
  new MidiPotentiometer(PIN_MULTIPLEXER_A, MIDI_CTRL_GAIN, MIDI_CHANNEL_ONE),
};

MidiThreeWaySwitch* three_way_switch = new MidiThreeWaySwitch(PIN_MULTIPLEXER_B, PIN_MULTIPLEXER_B, MIDI_CTRL_CUE_MODE, MIDI_CHANNEL_CUE);

BaseComponent* components_multiplexer_b[] = {
  new MidiPotentiometer(PIN_MULTIPLEXER_B, MIDI_CTRL_GAIN, MIDI_CHANNEL_BOOTH),
  new MidiPotentiometer(PIN_MULTIPLEXER_B, MIDI_CTRL_VOLUME, MIDI_CHANNEL_FOUR),
  new MidiPotentiometer(PIN_MULTIPLEXER_B, MIDI_CTRL_VOLUME, MIDI_CHANNEL_THREE),
  new MidiPotentiometer(PIN_MULTIPLEXER_B, MIDI_CTRL_VOLUME, MIDI_CHANNEL_TWO),
  new MidiPotentiometer(PIN_MULTIPLEXER_B, MIDI_CTRL_VOLUME, MIDI_CHANNEL_ONE),
  new MidiPotentiometer(PIN_MULTIPLEXER_B, MIDI_CTRL_GAIN, MIDI_CHANNEL_CUE),
  new MidiTwoWaySwitch(PIN_MULTIPLEXER_B, MIDI_CTRL_BYPASS_EQ_CUE, MIDI_CHANNEL_MAIN),
  0,
  0,
  new MidiButton(PIN_MULTIPLEXER_B, MIDI_CTRL_TOGGLE_CUE, MIDI_CHANNEL_FOUR),  
  new MidiButton(PIN_MULTIPLEXER_B, MIDI_CTRL_TOGGLE_CUE, MIDI_CHANNEL_THREE),  
  new MidiButton(PIN_MULTIPLEXER_B, MIDI_CTRL_TOGGLE_CUE, MIDI_CHANNEL_TWO),  
  new MidiButton(PIN_MULTIPLEXER_B, MIDI_CTRL_TOGGLE_CUE, MIDI_CHANNEL_ONE),  
  0,
  0,
  0
};

LED* LEDCueOne = new LED(PIN_LED_CUE_ONE);
LED* LEDCueTwo = new LED(PIN_LED_CUE_TWO);
LED* LEDCueThree = new LED(PIN_LED_CUE_THREE);
LED* LEDCueFour = new LED(PIN_LED_CUE_FOUR);


Neopixel* neopixels[] = {
  new Neopixel(PIN_LED_NEOPIXEL_A),
  new Neopixel(PIN_LED_NEOPIXEL_B),
  new Neopixel(PIN_LED_NEOPIXEL_C),
  new Neopixel(PIN_LED_NEOPIXEL_D),
  new Neopixel(PIN_LED_NEOPIXEL_E),
};


BaseComponent* components[] = {
  LEDCueOne,
  LEDCueTwo,
  LEDCueThree,
  LEDCueFour
};

void setup() { 
  Serial.begin(115200);
  multiplexer->setup();


  for(const auto neopixel : neopixels) {
    neopixel->setup();
  }

  for(const auto component : components) {
    component->setup();
  }

}
// Waits till one byte is ready to read
int readOneByte() {
  while (Serial.available() == 0) {};
  return Serial.read();
}

void sysex_cleanup() {
  while (Serial.available() >= 3) {
    int byte = readOneByte();
    if (byte == MIDI_SYSEX_END) break;
  }
}

void sysex_send_identity_reply(int sysex_channel) {
  DBG("MIDI IN: SYSEX: Sending identity");
  Serial.write(MIDI_SYSEX);
  Serial.write(MIDI_SYSEX_TYPE_NON_REALTIME);
  Serial.write(sysex_channel);
  Serial.write(MIDI_SYSEX_GENERAL_INFORMATION);
  Serial.write(MIDI_SYSEX_REPLY_IDENTITY);

  Serial.write(IDENTITY_MANUFACTURER_ID);
  Serial.write(IDENTITY_FAMILY_CODE_ONE);
  Serial.write(IDENTITY_FAMILY_CODE_TWO);
  Serial.write(IDENTITY_MODEL_NUMBER_ONE);
  Serial.write(IDENTITY_MODEL_NUMBER_TWO);
  Serial.write(IDENTITY_VERSION_NUMBER_ONE);
  Serial.write(IDENTITY_VERSION_NUMBER_TWO);
  Serial.write(IDENTITY_VERSION_NUMBER_THREE);
  Serial.write(IDENTITY_VERSION_NUMBER_FOUR);

  Serial.write(MIDI_SYSEX_END);
}

void process_midi_read() {
  if (Serial.available() == 0) return;
  //DBG("process_midi_read()");
  int first_byte = readOneByte();

  // Handle sysex
  if (first_byte == MIDI_SYSEX) {
    // Implement sysex identity request
    // http://midi.teragonaudio.com/tech/midispec/identity.htm
    DBG("MIDI IN: SYSEX");
    int sysex_type = readOneByte();
    int sysex_channel = readOneByte();
    int sub_id = readOneByte();
    if (sub_id != MIDI_SYSEX_GENERAL_INFORMATION) {
      DBG("MIDI IN: SYSEX: ERROR: Unknown sub_id");
      sysex_cleanup();
      return;
    }
    int sub_id2 = readOneByte();
    if (sub_id2 != MIDI_SYSEX_REQUEST_IDENTITY) {
      DBG("MIDI IN: SYSEX: ERROR: Unknown sub_id2");
      sysex_cleanup();
      return;
    }
    if (readOneByte() != 0xF7) {
      DBG("MIDI IN: SYSEX: ERROR: Request did not end with MIDI_SYSEX_END");
      sysex_cleanup();
      return;
    }
    // We arrived at a successful midi sysex request for identity
    sysex_send_identity_reply(sysex_channel);
    
    return;
  }

  int note = readOneByte();
  int value = readOneByte();
  int cmd = first_byte >> 4;
  int channel = first_byte & 0x0F;

  DBG("MIDI IN: %#02x %i %i %i", cmd, channel, note, value);

  if (cmd != MIDI_COMMAND_BUTTON) {
    //DBG("INVALID MIDI: Unsupported midi command %i", cmd);
    return;
  }

  if (channel < MIDI_CHANNEL_ONE || channel > 4) {
    DBG("INVALID MIDI: Channel must be between 0 and 4.");
    return;
  }

  if (note == MIDI_CTRL_VOLUME) {
    if (value > 10) { 
      DBG("INVALID MIDI: Value must be between 0 and 10.");
      return;
    }

    auto neopixel = neopixels[channel];
    auto rgb_color = VUMETER_COLORS[value];
    neopixel->setColor(
      rgb_color[0],
      rgb_color[1],
      rgb_color[2]
    );
    //DBG("Set neopixel %i to %i %i;%i,%i", channel, value, rgb_color[0], rgb_color[1], rgb_color[2]);
    return;
  } else if (note == MIDI_CTRL_TOGGLE_CUE) {
    if (value > 1) { 
      DBG("INVALID MIDI: Value must be between 0 and 1.");
      return;
    }

    if (channel < MIDI_CHANNEL_ONE || channel > MIDI_CHANNEL_FOUR) { 
      DBG("INVALID MIDI: Channel must be between 1 and 4.");
      return;
    }

    int led_i = channel;
    DBG("Turning on LED %i", led_i);

    LED* led = components[led_i];
    led->setState(value == 0 ? false : true);
    return;
  } else {
    DBG("INVALID MIDI: Command not supported.");
    DBG("MIDI IN: %i %i %i %i", cmd, channel, note, value);
    return;
  }
}


int neopixel_i = 0;

unsigned long last_forced_send_midi = 0;

// Force sending the current midi value every second
const unsigned long FORCE_SEND_MIDI_DELTA = 1000;

void loop() {
  process_midi_read();

  const unsigned long now = millis();
  const bool force_send_midi = last_forced_send_midi == 0 || now - last_forced_send_midi > FORCE_SEND_MIDI_DELTA;
  if (force_send_midi) {
    last_forced_send_midi = now;
  }
  
  for (int i = 0; i<15; i++) {
    multiplexer->select(i);

    // Components on multiplexer A
    if (
        i < 15
      ) {
      auto component = components_multiplexer_a[i];
      component->process(force_send_midi);
    }
    
    // Components on multiplexer B

    if (i < 15) {
      if (i == 7) {
        three_way_switch->_process_pin_a(digitalRead(PIN_MULTIPLEXER_B));
      } else if (i == 8) {
        three_way_switch->_process_pin_b(digitalRead(PIN_MULTIPLEXER_B));
      } else {
        auto component = components_multiplexer_b[i];
        if (component != 0) component->process(force_send_midi);
      }
    }
  }

  neopixels[neopixel_i]->process();
  neopixel_i = neopixel_i + 1;
  if (neopixel_i > 4) neopixel_i = 0;
}
