#pragma once

#include <Arduino.h>

#ifndef DEBUG_H
  #define DEBUG_H

  //#define ENABLE_DEBUG // Uncomment to enable debugging
  #ifdef ENABLE_DEBUG 
    #define IFDEBUG(x) x
    #define IFNDEBUG(x)
    #define DBG(fmt, ...) _SerialPrintf(PSTR(fmt), ##__VA_ARGS__)
  #else 
    #define IFDEBUG(x)
    #define IFNDEBUG(x, ...) x
    #define DBG(fmt, ...)
  #endif
#endif

int serialputc(char c, FILE *fp);
void _SerialPrintf(const char *fmt, ...);
