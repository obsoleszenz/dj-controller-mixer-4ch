#include <Arduino.h>

#include "constants.h"
#include "vumeter-colors.h"
#include "log.h"

#include "hardware/Multiplexer.h"

#include "midi/MidiPotentiometer.h"
#include "midi/MidiButton.h"
#include "midi/MidiTwoWaySwitch.h"
#include "midi/MidiThreeWaySwitch.h"
#include "hardware/LED.h"
#include "hardware/Neopixel.h"
