#include "LED.h"


LED::LED(int pin_led) : pin_led(pin_led), state(false) {
  //DBG("LED:LED() pin_led: %i", pin_led);
}

void LED::setup() {
  //DBG("LED::setup() pin_led: %i", pin_led);
  pinMode(pin_led, OUTPUT);   
}

void LED::process(bool force_send_midi) {}

void LED::setState(bool on) {
  digitalWrite(pin_led, on ? HIGH : LOW);
  state = on;
}

bool LED::getState() {
  return state;
}
