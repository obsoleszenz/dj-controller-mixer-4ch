#pragma once

#include "../log.h"
#include "BaseComponent.h"
#include "PinDebounce.h"


class ThreeWaySwitch : public BaseComponent {
  public:
    int pin_switch_a; 
    int pin_switch_b; 
    PinDebounce pin_debounce_a;
    PinDebounce pin_debounce_b;

    ThreeWaySwitch(int pin_switch_a, int pin_switch_b);
    void setup();
    void process(bool force_send_midi);
    void _process(int pin_a_value, int pin_b_value);
    void _process_pin_a(int pin_a_value);
    void _process_pin_b(int pin_b_value);
    virtual void onStateOne();
    virtual void onStateTwo();
    virtual void onStateThree();
};
