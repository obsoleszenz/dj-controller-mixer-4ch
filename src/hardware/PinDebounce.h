#pragma once

#include "log.h"

bool isBouncing(int pin_value, int* last_bounce_pin_value, unsigned long* last_bounce_millis);
enum class PinState {High, Unchanged, Low};

int pinStateToInt(PinState pin_state);

class PinDebounce {
  public: 
    bool was_high = false;
    unsigned long last_bounce_millis = -1;
    int last_bounce_pin_value;

    PinState state;

    PinDebounce();
    PinState debounce(int pin_value);
    bool isHigh();
};
