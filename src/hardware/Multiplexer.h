#pragma once

#include <Arduino.h>

#include "BaseComponent.h"
#include "log.h"

class Multiplexer : public BaseComponent {
  public:
    Multiplexer();
    void setup();
    void process(bool force_send_midi);
    void select(unsigned int select);

};

