#include "Button.h"



Button::Button(int pin_button) : pin_button(pin_button) {}

void Button::setup() {
  pinMode(pin_button, INPUT_PULLUP);
}

void Button::process(bool force_send_midi) {
  int pin_value = digitalRead(pin_button);

  _process(pin_value);
}

void Button::_process(int pin_value) {
  PinState pin_state = pin_debounce.debounce(pin_value);
  if(pin_state == PinState::Low) onPress();
  if(pin_state == PinState::High) onRelease();
}

void Button::onPress() {
  //DBG("[Button] %i Pressed!", pin_button);
}
void Button::onRelease() {
  //DBG("[Button] %i Released!", pin_button);
}
