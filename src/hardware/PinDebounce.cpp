#include "PinDebounce.h"

const int BOUNCING_TRESHOLD = 30;

int pinStateToInt(PinState pin_state) {
  if (pin_state == PinState::High) {
    return 1;
  } else if (pin_state == PinState::Low) {
    return 0;
  }
  return -1;
}

bool isBouncing(int pin_value, int* last_bounce_pin_value, unsigned long* last_bounce_millis) {
  DBG("isBouncing pointer adresses: %i %i", last_bounce_pin_value, last_bounce_millis);
  if (*last_bounce_pin_value == pin_value) {
    return false;
  }
  unsigned long current_flake_millis = millis();
  //println("current_flake %lu last_flake %lu", current_flake, *last_bounce_millis);
  bool isBouncing = (current_flake_millis - *last_bounce_millis) < BOUNCING_TRESHOLD ? true : false;
  if (isBouncing) {
    *last_bounce_millis = current_flake_millis;
  }
  *last_bounce_pin_value = pin_value;
  return isBouncing;
}

PinDebounce::PinDebounce() {} 

PinState PinDebounce::debounce(int pin_value) {
  IFDEBUG(
    unsigned long time = millis();
  );
  if(pin_value == LOW && was_high == false) {
    bool is_bouncing = isBouncing(pin_value, &last_bounce_pin_value, &last_bounce_millis);
    IFDEBUG(
      Serial.println(time + String(" isBouncing ") + (is_bouncing ? "true" : "false"))
    );
    if(is_bouncing) {
      state = PinState::Unchanged;
    } else {
      was_high = true;
      state = PinState::High;
    }
    DBG("%i state: %i", time, state);
  } else if(pin_value == HIGH && was_high == true){
    bool is_bouncing = isBouncing(pin_value, &last_bounce_pin_value, &last_bounce_millis);
    IFDEBUG(
      Serial.println(time + String(" isBouncing ") + (is_bouncing ? "true" : "false"))
    );
    if(is_bouncing) {
      state = PinState::Unchanged;
    } else {
      was_high = false;
      state = PinState::Low;
    }
    IFDEBUG(
      Serial.println(time + String(" state: ") + pinStateToInt(state))
    );
  } else {
    state = PinState::Unchanged;
  }

  return state;
}

bool PinDebounce::isHigh() {
  return was_high;
}
