#include "ThreeWaySwitch.h"


ThreeWaySwitch::ThreeWaySwitch(int pin_switch_a, int pin_switch_b) : pin_switch_a(pin_switch_a), pin_switch_b(pin_switch_b) {}

void ThreeWaySwitch::setup() {
  pinMode(pin_switch_a, INPUT_PULLUP);
  pinMode(pin_switch_b, INPUT_PULLUP);
}

void ThreeWaySwitch::process(bool force_send_midi) {
  int pin_value_a = digitalRead(pin_switch_a);
  int pin_value_b = digitalRead(pin_switch_b);

  _process(pin_value_a, pin_value_b);
}

void ThreeWaySwitch::_process(int pin_value_a, int pin_value_b) {
}

void ThreeWaySwitch::_process_pin_a(int pin_value_a) {
  pin_debounce_a.debounce(pin_value_a);
}

void ThreeWaySwitch::_process_pin_b(int pin_value_b) {
  pin_debounce_b.debounce(pin_value_b);

  if (pin_debounce_a.state == PinState::Unchanged && pin_debounce_b.state == PinState::Unchanged) return;

  if (pin_debounce_a.isHigh() && !pin_debounce_b.isHigh()) {
    onStateThree();
  } else if (!pin_debounce_a.isHigh() && pin_debounce_b.isHigh()) {
    onStateOne();
  } else {
    onStateTwo();
  }
}


void ThreeWaySwitch::onStateOne() {
  DBG("[ThreeWaySwitch] %i;%i StateOne!", pin_switch_a, pin_switch_b);
}
void ThreeWaySwitch::onStateTwo() {
  DBG("[ThreeWaySwitch] %i;%i StateTwo!", pin_switch_a, pin_switch_b);
}

void ThreeWaySwitch::onStateThree() {
  DBG("[ThreeWaySwitch] %i;%i StateThree!", pin_switch_a, pin_switch_b);
}
