#pragma once

#include <Arduino.h>

#include "BaseComponent.h"
#include "../log.h"
#include "PinDebounce.h"


class TwoWaySwitch : public BaseComponent {
  public:
    int pin_switch; 
    PinDebounce pin_debounce;
    TwoWaySwitch(int pin_switch);
    void setup();
    void process(bool force_send_midi);
    void _process(int pin_value);
    virtual void onActivate();
    virtual void onDeactivate();
};
