#include "Neopixel.h"

Neopixel::Neopixel(int pin) :
  pin(pin),
  neopixel(new Adafruit_NeoPixel(1, pin, NEO_RGB + NEO_KHZ800))
{}

void Neopixel::setup() {
  pinMode(pin, OUTPUT);
  neopixel->begin();
  neopixel->clear();
  neopixel->show();
}

void Neopixel::setColor(uint8_t r, uint8_t g, uint8_t b) {
  if (r == curr_r && g == curr_g && b == curr_b) return;
  curr_r = r;
  curr_g = g;
  curr_b = b;
  showOnNextProcess = true;
  DBG("Neopixel: on pin %i should update on next", pin);
}

void Neopixel::process(bool force_send_midi) {
  if (showOnNextProcess == false) return;
  if (neopixel->canShow() == false) return;
  DBG("Neopixel: updating pin %i", pin);
  neopixel->setPixelColor(0, curr_r, curr_g, curr_b);
  neopixel->show();
  showOnNextProcess = false;
}
