#pragma once

class BaseComponent {
  public:
    virtual void setup() = 0;
    virtual void process(bool force_send_midi) = 0;
};
