#pragma once

#include <Adafruit_NeoPixel.h>
#include "../log.h"
#include "BaseComponent.h"

class Neopixel : public BaseComponent {
  public:
    int pin; 
    uint8_t curr_r;
    uint8_t curr_g;
    uint8_t curr_b;
    bool showOnNextProcess = false;
    Adafruit_NeoPixel* neopixel;
    Neopixel(int pin);
    void setup();
    void setColor(uint8_t r, uint8_t g, uint8_t b);
    void process(bool force_send_midi = false);
};
