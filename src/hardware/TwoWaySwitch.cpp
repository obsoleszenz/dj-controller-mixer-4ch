#include "TwoWaySwitch.h"


TwoWaySwitch::TwoWaySwitch(int pin_switch) : pin_switch(pin_switch) {}

void TwoWaySwitch::setup() {
  pinMode(pin_switch, INPUT_PULLUP);
}

void TwoWaySwitch::process(bool force_send_midi) {
  int pin_value = digitalRead(pin_switch);

  _process(pin_value);
}

void TwoWaySwitch::_process(int pin_value) {
  PinState pin_state = pin_debounce.debounce(pin_value);
  if(pin_state == PinState::High) onActivate();
  if(pin_state == PinState::Low) onDeactivate();
}

void TwoWaySwitch::onActivate() {
  DBG("[TwoWaySwitch] %i Activated!", pin_switch);
}
void TwoWaySwitch::onDeactivate() {
  DBG("[TwoWaySwitch] %i Deactivated!", pin_switch);
}
