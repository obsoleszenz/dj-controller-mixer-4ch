#pragma once

#include "../log.h"
#include "PinDebounce.h"
#include "BaseComponent.h"

class Button : public BaseComponent {
  public:
    int pin_button; 
    PinDebounce pin_debounce;
    Button(int pin_button);
    void setup();
    void process(bool force_send_midi);
    void _process(int pin_value);
    virtual void onPress();
    virtual void onRelease();
};
