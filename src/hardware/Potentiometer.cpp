#include "Potentiometer.h"

Potentiometer::Potentiometer(int pin) : pin(pin) {}
  

void Potentiometer::setup() {
  pinMode(pin, INPUT);
}

void Potentiometer::process(bool force_send_midi) {
  int value = analogRead(pin);

  if (smoothedValue == -1) {
    smoothedValue = value;
  } else {
    // Smooth the analog value with some mathgic
    int smoothedDifference = (value - smoothedValue) / 4;
    smoothedValue += smoothedDifference;
    if (!force_send_midi && smoothedDifference == 0) return;
    
  }
  //println("Potentiometer [%i] %i", pin, value);

  onChange(value, force_send_midi);
}
