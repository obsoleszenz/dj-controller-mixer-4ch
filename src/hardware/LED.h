#pragma once

#include "../log.h"
#include "BaseComponent.h"

class LED : public BaseComponent {
  public:
    int pin_led; 
    bool state;
    LED(int pin_led);
    void setup();
    void process(bool force_send_midi);
    void setState(bool);
    bool getState();
};

