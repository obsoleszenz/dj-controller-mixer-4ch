#!/usr/bin/python
import sys
import serial
import time

def sendMidi(serialDevice, command, channel, note, value):
    first_byte = (command << 4) + channel
    print("[MIDI ->] {} {} {}".format(hex(first_byte), hex(note), hex(value)))  
    serialDevice.write([first_byte, note, value])


def help():
    print ("""
send-serial-midi: <device> 

Example: send-serial-midi /dev/ttyUSB0""")


def string_to_int(string):
    integer = None
    try:
        integer = int(string)
    except ValueError:
        try:
            integer = int(string, base=16)
        except ValueError:
            pass

    if integer is None:
        raise Exception("Cannot parse " + string)
    return integer

def main():
    show_help = len(sys.argv) != 2
    
    parsed_argv = []
    if show_help is False:
        for i in range(2, len(sys.argv)):
            parsed_argv.append(string_to_int(sys.argv[i]))
    print(parsed_argv)

    if show_help is True:
        help()
        return
    device = sys.argv[1]
    serialDevice = serial.Serial(device, 115200, timeout=1, exclusive=False)
    color = 10
    while True:
        sendMidi(serialDevice, 0xB, 1, 3, color)
        time.sleep(0.01)


if __name__ == "__main__":
    main()


