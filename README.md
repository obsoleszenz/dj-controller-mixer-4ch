

# DjMixer - A 4 channel DJ MIDI  Mixer

## Description

This project provides the firmware and build instructions for a 4 channel MIDI DJ Mixer. It offers 2 sends per channel and a cue system. It has no EQ section.

## Pictures

<img src="https://codeberg.org/obsoleszenz/DjMixer/raw/commit/d3c04709aadee77feee626520db10620434a122e/docs/hardware-1.png" alt="Picture of the finished controller" height="300px"/>

<img src="https://codeberg.org/obsoleszenz/DjMixer/raw/commit/65cb7436831a187fbcd28be2c2089bf455023451/docs/hardware-2.png" alt="Picture of debugging the controller" height="300px"/>

